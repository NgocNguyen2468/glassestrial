import React, { Component } from 'react'
import Product from "../Product";

let arrProduct = [

    { id: 1, price: 30, name: 'GUCCI G8850U', url: './img/Copy of v1.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 2, price: 50, name: 'GUCCI G8759H', url: './img/Copy of v2.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 3, price: 30, name: 'DIOR D6700HQ', url: './img/Copy of v3.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 4, price: 30, name: 'DIOR D6005U', url: './img/Copy of v4.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 5, price: 30, name: 'PRADA P8750', url: './img/Copy of v5.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 6, price: 30, name: 'PRADA P9700', url: './img/Copy of v6.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 7, price: 30, name: 'FENDI F8750', url: './img/Copy of v7.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 8, price: 30, name: 'FENDI F8500', url: './img/Copy of v8.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 9, price: 30, name: 'FENDI F4300', url: './img/Copy of v9.png', desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

];

class GlassTrial extends Component {

    renderProduct = () => {
        return arrProduct.map((product, index) => {
            return (
                <div className="col-lg-1 w-100" change={product.url} key={index + 1}>
                    <Product item={product} />
                </div>
            );
        });
    };

    state = {
        // id: null,
        url: null
    };

    tryGlass = (img) => () => {
        this.setState(
            {
                // id: index,
                url: img

            },
            () => {
                console.log(this.state.url);
                // console.log(this.state.id);
            }
        );
    };
    render() {
        return (

            <div className="container">
                <h1>Try something</h1>
                <div>
                    <img src="./img/Copy of model.jpg" className="mt-5 mb-5" style={{ height: 300, }} alt="" />
                    <img src={this.state.url} style={{ height: 50, marginLeft: -200, marginTop: -124 }} alt="" />
                </div>
                <div className="row mt-5">
                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v1.png')}>
                        <img src="./img/Copy of g1.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>

                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v2.png')} >
                        <img src="./img/Copy of g2.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-2 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v3.png')} >
                        <img src="./img/Copy of g3.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v4.png')}>
                        <img src="./img/Copy of g4.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-2 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v5.png')}>
                        <img src="./img/Copy of g5.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v6.png')} >
                        <img src="./img/Copy of g6.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-2 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v7.png')}>
                        <img src="./img/Copy of g7.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v8.png')}>
                        <img src="./img/Copy of g8.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>
                    <div className="col-lg-1 w-100" style={{ cursor: "pointer" }} onClick={this.tryGlass('./img/Copy of v9.png')}>
                        <img src="./img/Copy of g9.jpg" style={{ height: 50, border: "1px solid blue" }} alt="" />
                    </div>

                </div>

            </div>

        )
    }
}

export default GlassTrial;